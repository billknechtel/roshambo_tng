RO SHAM BO: The Next Generation
===============================

An exercise in Symfony, PSR-2, and Trek Jokes.  The basic premise is to just flex Symfony a bit.  Provide a silly game to play, keep some stats in a database, flesh out some minimal models and controllers, etc. In this particular instance, I also spent time making sure the PHPDoc comments were full and complete. Of course now this version of Symfony is well out of date, but the exercise was a fun one, and shows off some mastery of the fundamentals... so I keep it around.

This code is live at [http://roshambotng.endikos.com](http://roshambotng.endikos.com)