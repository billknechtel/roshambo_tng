<?php
namespace AppBundle\Service;

class RoShamBoTng
{
    /**
     * This array allows us to use a simple if statement to check two chosen
     * moves against each other. The array keys are the valid moves, the value
     * arrays contain the moves the key beats. This way, if
     * $rulesMatrix[Player 1 Move][Player 2 Move] exists, then player 2 has
     * lost. Otherwise, player 2 has won.
     *
     * @var array Multidimensional array that contains the basic game rules
     */
    private static $rulesMatrix = array(
        'rock' => array('lizard', 'scissors'),
        'paper' => array('rock', 'spock'),
        'scissors' => array('paper', 'lizard'),
        'spock' => array('rock', 'scissors'),
        'lizard' => array('spock', 'paper')
    );

    /**
     * Supply the chosen moves and we'll tell you who won. An array will be returned.
     * The first key will be "success" and will be either true or false.  False
     * indicates there was a problem with the input. If false, a second key, "message"
     * will be available to explain what went wrong.  True indicates that the inputs
     * are good and we either had a winner or a draw. If "success" is true, a "winner"
     * key will be available, and will return an int of 0, 1, or 2. 0 means draw, 1 or 2
     * is the player number that won.
     *
     * @param string $playerOneMove Indicates player one's move
     * @param string $playerTwoMove Indicates player two's move
     * @return array Will have a "success" key, and either a "message" or "winner" key
     */
    public static function playGame($playerOneMove, $playerTwoMove)
    {
        // Our rulesMatrix is all lower-case...
        $playerOneMove = strtolower($playerOneMove);
        $playerTwoMove = strtolower($playerTwoMove);

        // Validate inputs
        if (!RoShamBoTng::isValidChoice($playerOneMove) || !RoShamBoTng::isValidChoice($playerTwoMove)) {
            return array(
                'success' => false,
                'message' => 'A player made an invalid move.'
            );
        }

        // Determine winner
        if ($playerOneMove == $playerTwoMove) {
            // We have a draw
            $winner = 0;
        } elseif (in_array($playerTwoMove, RoShamBoTng::$rulesMatrix[$playerOneMove])) {
            $winner = 1;
        } else {
            $winner = 2;
        }

        return array(
            'success' => true,
            'winner' => $winner
        );
    }

    /**
     * Internal use. Just to simply validate that we're receving a string and
     * it's a valid move.
     *
     * @param string $choice Denotes chosen move
     * @return bool
     */
    private static function isValidChoice($choice)
    {
        $validChoices = array_keys(RoShamBoTng::$rulesMatrix);
        return in_array($choice, $validChoices) ? true : false;
    }
}
