<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GameLog: Each entry represents a game. It will tell you what each player
 * moved, and who won.
 *
 * By Default, when the object is instantiated, the playedAt property is
 * set to "Now".
 *
 * @ORM\Table(name="game_log")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameLogRepository")
 */
class GameLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="played_at", type="datetime")
     */
    private $playedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="player1_move", type="string", length=8)
     */
    private $player1Move;

    /**
     * @var string
     *
     * @ORM\Column(name="player2_move", type="string", length=8)
     */
    private $player2Move;

    /**
     * @var string
     *
     * @ORM\Column(name="winner", type="string", length=8)
     */
    private $winner;

    public function __construct()
    {
        $this->playedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set playedAt
     *
     * @param \DateTime $playedAt
     * @return GameLog
     */
    public function setPlayedAt($playedAt)
    {
        $this->playedAt = $playedAt;

        return $this;
    }

    /**
     * Get playedAt
     *
     * @return \DateTime 
     */
    public function getPlayedAt()
    {
        return $this->playedAt;
    }

    /**
     * Set player1Move
     *
     * @param string $player1Move
     * @return GameLog
     */
    public function setPlayer1Move($player1Move)
    {
        $this->player1Move = $player1Move;

        return $this;
    }

    /**
     * Get player1Move
     *
     * @return string 
     */
    public function getPlayer1Move()
    {
        return $this->player1Move;
    }

    /**
     * Set player2Move
     *
     * @param string $player2Move
     * @return GameLog
     */
    public function setPlayer2Move($player2Move)
    {
        $this->player2Move = $player2Move;

        return $this;
    }

    /**
     * Get player2Move
     *
     * @return string 
     */
    public function getPlayer2Move()
    {
        return $this->player2Move;
    }

    /**
     * Set winner
     *
     * @param string $winner
     * @return GameLog
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;

        return $this;
    }

    /**
     * Get winner
     *
     * @return string 
     */
    public function getWinner()
    {
        return $this->winner;
    }
}
