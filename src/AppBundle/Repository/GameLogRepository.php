<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * GameLogRepository: Utility methods for gathering statistics
 */
class GameLogRepository extends EntityRepository
{
    /**
     * Returns number of times a given user has won
     *
     * @param string $userName
     * @return array Single 'wins' key indicates user win count
     */
    public function getWinCountsForUser($userName)
    {
        return $this->createQueryBuilder('g')
            ->select('COUNT(g) AS wins')
            ->where('g.winner = :winner')
            ->setParameter('winner', $userName)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Returns number of times a specified moves has been used for a user
     *
     * @param string $moveName
     * @param int $playerNumber
     * @return array Single 'moves' key indicates number of times the moves been used
     */
    public function getMoveCountForUser($moveName, $playerNumber)
    {
        // Note: this may appear on the surface to be exposing an exploit,
        // but playerNumber is not settable from the outside world - it gets
        // its value from the private property in our controller - so while
        // non-conventional, this is safe to use, and keeps us nice and DRY.
        return $this->createQueryBuilder('g')
            ->select('COUNT(g) AS moves')
            ->where('g.player' . $playerNumber . 'Move = :move')
            ->setParameter('move', $moveName)
            ->getQuery()
            ->getSingleResult();
    }
}
