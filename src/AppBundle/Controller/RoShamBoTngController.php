<?php

namespace AppBundle\Controller;

use AppBundle\Service\RoShamBoTng;
use AppBundle\Entity\GameLog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class RoShamBoTngController extends Controller
{
    /**
     * @var array Valid choices for players
     */
    private $moveChoices = array('Rock', 'Paper', 'Scissors', 'Spock', 'Lizard');

    /**
     * The keys (indexes) in this array match the player numbers that are retuned
     * by RoShamBoTng::playGame(). This makes it simple to do a name lookup.
     *
     * @var array Player names
     */
    private $playerNames = array('Draw', 'Computer', 'Human');

    /**
     * Present user with their choices and current game stats
     *
     * @Route("/", name="homepage")
     * @Template("@App/RoShamBoTng/index.html.twig")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        // Send game data and stats
        return array(
            'choices' => $this->moveChoices,
            'gameStats' => $this->getGameStatistics(),
            'players' => $this->playerNames,
            'moveChoices' => $this->moveChoices
        );
    }

    /**
     * Accept the user's choice, generate a computer choice, play the game,
     * record and present results and stats.
     *
     * @Route("/results", name="results")
     * @Template("@App/RoShamBoTng/results.html.twig")
     * @Method({"POST"})
     */
    public function resultsAction(Request $request)
    {
        // Choose the computer's move.  We'll use mt_rand() to efficiently pick
        // from the available moves. In a perfect world, I'd seed the function
        // with atmospheric noise or something similar (think random.org) for
        // cryptographically sound randomness, but that's beyond the
        // requested scope :-)

        // This could be done in one line, but for legibility's sake...
        $choiceArrayLength = count($this->moveChoices) - 1;
        $computerChoice = $this->moveChoices[mt_rand(0, $choiceArrayLength)];

        $humanChoice = $request->request->get('choice');

        // Play the game
        $game = RoShamBoTng::playGame($computerChoice, $humanChoice);

        // Figure out what to show the human player
        if ($game['success']) {
            if ($game['winner'] == 0) {
                $result = "It's a draw! Both players chose identically.";
            } else {
                $result = 'And the ' . $this->playerNames[$game['winner']] . ' is the winner!';
            }
        } else {
            $result = 'There was a problem. The reported error is: ' . $game['message'];
        }

        // Record the results of this game in the database
        // Note that playedAt is automatically set on instantiation
        $gameLogEntry = new GameLog();
        $gameLogEntry->setPlayer1Move($computerChoice);
        $gameLogEntry->setPlayer2Move($humanChoice);
        $gameLogEntry->setWinner($this->playerNames[$game['winner']]);

        $em = $this->getDoctrine()->getManager();
        $em->persist($gameLogEntry);
        $em->flush();

        // Send the results, player and move data, and stats to the results page.
        return array(
            'result' => $result,
            'playerOne' => array('name' => $this->playerNames[1], 'choice' => $computerChoice),
            'playerTwo' => array('name' => $this->playerNames[2], 'choice' => $humanChoice),
            'gameStats' => $this->getGameStatistics(),
            'players' => $this->playerNames,
            'moveChoices' => $this->moveChoices
        );
    }

    /**
     * Build an array based on the data in the game log.  Pass it back to the
     * caller as a multidimensional array, with player name as the first key,
     * wins and move totals as secondary keys, and data as the third elements.
     *
     * So you've got, for instance:
     * $statistics['Computer']['wins'] which contains total win count, and
     * $statistics['Computer']['moveTotals']['Spock'] which would contain
     * the total number of times the computer has played the Spock move.
     *
     * @return array Multidimensional array with statistical data
     */
    private function getGameStatistics()
    {
        // Prep our return array
        $statistics = array();

        // Grab a connection to the DB
        $em = $this->getDoctrine()->getManager();

        // Gather stats
        foreach ($this->playerNames as $id => $playerName) {
            // Array prep
            $statistics[$playerName] = array();
            $repo = $em->getRepository('AppBundle:GameLog');

            // Win Counts
            $num = $repo->getWinCountsForUser($playerName);
            $statistics[$playerName]['wins'] = $num['wins'];

            // Move counts
            if ($id == 0) {
                // The special "Draw" user name doesn't get any moves
                continue;
            }
            $statistics[$playerName]['moveTotals'] = array();
            foreach ($this->moveChoices as $moveName) {
                $num = $repo->getMoveCountForUser($moveName, $id);
                $statistics[$playerName]['moveTotals'][$moveName] = $num['moves'];
            }
        }

        return $statistics;
    }

}
